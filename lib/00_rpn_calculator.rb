class RPNCalculator

  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def value
    @stack[-1]
  end

  def plus
    raise "calculator is empty" if @stack.length < 2
    sum = @stack.pop + @stack.pop
    @stack << sum
  end

  def minus
    raise "calculator is empty" if @stack.length < 2
    first_number = @stack.pop
    difference =  @stack.pop - first_number
    @stack << difference
  end

  def divide
    raise "calculator is empty" if @stack.length < 2
    divisor = @stack.pop.to_f
    quotient =  @stack.pop.to_f / divisor
    @stack << quotient
  end

  def times
    raise "calculator is empty" if @stack.length < 2
    product = @stack.pop * @stack.pop
    @stack << product
  end

  OPERATORS = %w(+ - * /)

  def tokens(string)
    tokenized = string.split
    tokenized.map! do |token|
      if OPERATORS.include?(token)
        token.to_sym
      else
        token.to_i
      end
    end
    tokenized
  end

  def evaluate(string)
    tokenized = tokens(string)
    tokenized.each do |token|
      case token
      when Integer then push(token)
      when :+ then plus
      when :- then minus
      when :/ then divide
      when :* then times
      end
    end
    value
  end

end
